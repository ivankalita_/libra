'use strict';

import Start from '../components/Start'
import New from '../components/New'
import NotFound from '../components/NotFound'

const routers = [
    {
        path: '/',
        component: Start
    },
    {
        path: '/new',
        component: New
    },
    {
        path: '*',
        component: NotFound
    }
]


export default routers;