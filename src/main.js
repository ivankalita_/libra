import Vue from 'vue'
import Router from 'vue-router'
import BootstrapVue from 'bootstrap-vue'



import App from './App.vue'
import Route from './router'
import './styles/main.scss'

Vue.use(Router)
Vue.use(BootstrapVue)

var router = new Router({
  mode: 'history',
  routes: Route
})


new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
