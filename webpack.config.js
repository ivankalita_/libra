var path = require('path')
var webpack = require('webpack')
var HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
  entry: [
    './src/main.js',
    './src/styles/main.scss',
    './src/index.pug',
  ],
  
  output: {
    path: path.resolve(__dirname, './dist'),
    publicPath: '/dist/',
    filename: 'build.js'
  },

  plugins: [
    new HtmlWebpackPlugin({
      title: 'ASD',
      template: path.resolve(__dirname, 'index.html')
    })
  ],

  module: {
    rules: [
      {
        test: /\.pug$/,
        // use: [
        //   'pug-loader'
        // ]
        loader: 'pug-loader',
        options: {
          pretty: true
        }
      },
      {
        test: /\.scss$/,
        include: path.resolve(__dirname, 'src/styles'),
        use: [
          'style-loader', // добавляет стили в DOM-дерево при помощи тега ˂style˃
          'css-loader', // добавляет CSS модули в граф зависимостей
          'sass-loader' // комилирует SCSS в CSS
        ]
      },
      {
        test: /\.css$/,
        use: [
          'vue-style-loader',
          'css-loader'
        ],
      },      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          loaders: {
          }
          // other vue-loader options go here
        }
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/
      },
      {
        test: /\.(png|jpg|gif|svg)$/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]?[hash]'
        }
      }
    ]
  },
  resolve: {
    alias: {
      'vue$': 'vue/dist/vue.esm.js'
    },
    extensions: ['*', '.js', '.vue', '.json']
  },
  devServer: {
    // contentBase: path.resolve(__dirname, 'dist/index.html'),
    historyApiFallback: true,
    publicPath: '/dist/',
    // color: true,
    noInfo: true,
    overlay: true,
    // progress: true,
    allowedHosts: [
      'habr.ru'
    ],
    port: 9000
  },
  performance: {
    hints: false
  },
  devtool: '#eval-source-map'
}

if (process.env.NODE_ENV === 'production') {
  module.exports.devtool = '#source-map'
  // http://vue-loader.vuejs.org/en/workflow/production.html
  module.exports.plugins = (module.exports.plugins || []).concat([
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"production"'
      }
    }),
    new webpack.optimize.UglifyJsPlugin({
      sourceMap: true,
      compress: {
        warnings: false
      }
    }),
    new webpack.LoaderOptionsPlugin({
      minimize: true
    })
  ])
}
